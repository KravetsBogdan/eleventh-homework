let eyes = document.querySelectorAll('.fas');
let input = document.querySelectorAll('input');
let btn = document.querySelector('.btn');

eyes.forEach(eye => {
    eye.addEventListener('click', () => {
        let input = eye.previousElementSibling;
        console.log(eye);
        console.log(eye.previousElementSibling);
        if (input.type === "password") {
            input.type = "text";
        } else {
            input.type = "password";
        }
        if (eye.classList.contains('fa-eye-slash')) {
            eye.classList.remove('fa-eye-slash');
            eye.classList.add('fa-eye');
        } else {
            eye.classList.remove('fa-eye');
            eye.classList.add('fa-eye-slash');
        }
    })
})

btn.addEventListener('click', () => {
    let error = document.querySelector('.text-error');
    if (input[0].value == '' && input[1].value == '') {
        error.innerHTML = 'Введіть пароль в поля для вводу';
    } else if (input[0].value === input[1].value) {
        alert('You are welcome');
        error.innerHTML = '';
        input.forEach(el => el.value = '');
    } else {
        error.innerHTML = 'Потрібно ввести однакові значення';
    }
})
